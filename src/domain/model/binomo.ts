import { Channel, ISignal, Signal } from "./signal";

export interface IBinomo {
  host: string;
  email: string;
  password: string;
  balance: number;
  trades: ISignal[];
  amount: number;
  channel: Channel;
}

export class Binomo implements IBinomo {
  private _props: IBinomo;
  constructor(props: IBinomo) {
    this._props = props;
  }
  public static create(props: IBinomo): Binomo {
    return new Binomo(props);
  }
  public unmarshall(): IBinomo {
    return {
      host: this.host,
      email: this.email,
      password: this.password,
      balance: this.balance,
      trades: this.trades.map((trade) => trade.unmarshall()),
      amount: this.amount,
      channel: this.channel,
    };
  }
  get host(): string {
    return this._props.host;
  }
  get email(): string {
    return this._props.email;
  }
  get password(): string {
    return this._props.password;
  }
  get balance(): number {
    return this._props.balance;
  }
  get trades(): Signal[] {
    return this._props.trades.map((signal) => Signal.create(signal));
  }
  get amount(): number {
    return this._props.amount;
  }
  get channel(): Channel {
    return this._props.channel;
  }
}
