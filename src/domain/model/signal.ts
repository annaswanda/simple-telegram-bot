export enum Channel {
  EURUSD = "EURUSD",
  EURGBP = "EURGBP",
  AUDUSD = "AUDUSD",
}
export enum Position {
  BUY = "BUY",
  SELL = "SELL",
}
export enum Status {
  OPEN = "OPEN",
  CLOSE = "CLOSE",
}
export interface ISignal {
  channel: Channel;
  entryTime: Date;
  closeTime: Date;
  position: Position;
  status: Status;
  win: boolean;
}
export class Signal implements ISignal {
  private props: ISignal;
  constructor(props: ISignal) {
    this.props = props;
  }
  public static create(props: ISignal): Signal {
    return new Signal(props);
  }
  public unmarshall(): ISignal {
    return {
      channel: this.channel,
      entryTime: this.entryTime,
      closeTime: this.closeTime,
      position: this.position,
      status: this.status,
      win: this.win,
    };
  }
  public close(win: boolean): Signal {
    this.props.status = Status.CLOSE;
    this.props.win = win;
    return this;
  }
  get channel(): Channel {
    return this.props.channel;
  }
  get entryTime(): Date {
    return this.props.entryTime;
  }
  get closeTime(): Date {
    return this.props.closeTime;
  }
  get position(): Position {
    return this.props.position;
  }
  get status(): Status {
    return this.props.status;
  }
  get win(): boolean {
    return this.props.win;
  }
}
