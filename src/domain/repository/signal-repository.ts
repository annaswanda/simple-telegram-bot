import { ISignal } from "../model";

export type TSignalRepositorySubsriber = (signal: ISignal) => void;

export interface ISignalRepository {
  subscribe(fn: TSignalRepositorySubsriber): void;
  unsubscribe(fn: TSignalRepositorySubsriber): void;
}
