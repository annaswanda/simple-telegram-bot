import { ISignal, Signal } from "../domain/model";
import { TSignalRepositorySubsriber } from "../domain/repository";
import { IAutoTradeService } from "../domain/service/auto-trade-service";
import { SignalRepository } from "../persistences/signal-repository";

export type TAutoTradeServiceListener = (signal: Signal) => void;
export class AutoTradeService implements IAutoTradeService {
  private _listeners: TAutoTradeServiceListener[] = [];
  start(): void {
    console.log("Auto Trade Starting...");
    const signalRepository = new SignalRepository();
    signalRepository.subscribe(this.onUpdate);
  }
  private onUpdate: TSignalRepositorySubsriber = (signal: ISignal) => {
    this._listeners.forEach((listener) => listener(Signal.create(signal)));
  };
  public subscribe(listener: TAutoTradeServiceListener) {
    this._listeners.push(listener);
  }
  public unsubscribe(listener: TAutoTradeServiceListener) {
    this._listeners.splice(this._listeners.indexOf(listener));
  }
}
