import { ISignal } from "../domain/model";
import {
  ISignalRepository,
  TSignalRepositorySubsriber,
} from "../domain/repository";
import { Telegram } from "../infrastructure/telegram";

export class SignalRepository implements ISignalRepository {
  private _listeners: TSignalRepositorySubsriber[] = [];
  private _telegram = Telegram.getInstance();
  constructor() {
    console.log(this._listeners);
    this._telegram.subscribe(this.onUpdate.bind(this));
  }
  onUpdate(signal: ISignal) {
    this._listeners.forEach((listener) => listener(signal));
  }
  subscribe(fn: TSignalRepositorySubsriber): void {
    this._listeners.push(fn);
  }
  unsubscribe(fn: TSignalRepositorySubsriber): void {
    this._listeners.splice(this._listeners.indexOf(fn), 1);
  }
}
