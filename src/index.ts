import { Position } from "./domain/model";
import { Binomo } from "./infrastructure/binomo";
import { AutoTradeService } from "./services/auto-trade-service";

const binomo = Binomo.getInstance();
(async () => {
  await binomo.initialize();
  const autoTradeService = new AutoTradeService();
  autoTradeService.start();
  autoTradeService.subscribe(async (signal) => {
    await binomo.setChannel(signal.channel);
    if (signal.position == Position.BUY) {
      await binomo.buy();
    } else if (signal.position == Position.SELL) {
      await binomo.sell();
    }
    // console.clear();
    // console.log(`BALANCE =>`, await binomo.getBalance());
    // console.log(signal.unmarshall());
  });
})();
