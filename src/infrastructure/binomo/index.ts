import puppeteer from "puppeteer";
import { IBinomo } from "../../domain/model/binomo";
import { Channel } from "../../domain/model";
import fs from "fs";
import { Page, Browser, Protocol } from "puppeteer/lib/types";
export class Binomo {
  private static instance: Binomo;
  private declare browser: Browser;
  private declare page: Page;
  private declare cookies: Protocol.Network.Cookie[];
  private currentUrl = "";
  private initialBinomo: IBinomo = {
    host: "https://binomo-go.com",
    email: "adnaw.sanna@gmail.com",
    password: ".Adnawsanna",
    balance: 0,
    trades: [],
    amount: 14000,
    channel: Channel.EURUSD,
  };
  private declare authToken: string | undefined;
  private constructor() {}
  public async initialize() {
    return new Promise(async (res, rej) => {
      try {
        this.browser = await puppeteer.launch({
          headless: 'new',
          args: ["--no-sandbox", "--disable-setuid-sandbox"],
        });
        this.page = await this.browser.newPage();
        await this.useOldCookies();
        await this.page.goto(`${this.initialBinomo.host}`);
        this.currentUrl = await this.page.url();
        this.cookies = await this.saveCookies();
        this.authToken = await this.getAuthToken();
        if (!this.authToken) {
          await this.page.goto(`${this.initialBinomo.host}/auth`);
          await this.page.waitForSelector("input");
          const inputs = await this.page.$$("input");
          await inputs[0].type(this.initialBinomo.email);
          await inputs[1].type(this.initialBinomo.password);
          const button = await this.page.$('button[type="submit"]');
          await button?.click();
          await this.page.waitForNavigation({ waitUntil: "domcontentloaded" });
          this.currentUrl = await this.page.url();
          this.cookies = await this.saveCookies();
          this.authToken = await this.getAuthToken();
          if (this.currentUrl === `${this.initialBinomo.host}/trading`) {
            console.log("Trading NOW");
            res(true);
          }
        } else {
          console.log("Trading NOW");
          res(true);
        }
      } catch (e) {
        rej(e);
      }
    });
  }
  private async useOldCookies() {
    try {
      const oldCookies = JSON.parse(
        fs.readFileSync("./cookies.json").toString()
      );
      await this.page.setCookie(...oldCookies);
    } catch (e) {}
  }

  private async saveCookies() {
    this.cookies = await this.page.cookies();
    fs.writeFileSync("./cookies.json", JSON.stringify(this.cookies));
    return this.cookies;
  }
  private async getAuthToken() {
    return this.cookies.find((item) => item.name == "authtoken")?.value;
  }
  public static getInstance(): Binomo {
    if (!Binomo.instance) {
      Binomo.instance = new Binomo();
    }
    return Binomo.instance;
  }
  public async getBalance(): Promise<number> {
    await this.page.waitForSelector("#qa_trading_balance", { visible: true });
    const rawBalance = await this.page.$("#qa_trading_balance");
    const formattedBalance = <string>(
      await (await rawBalance?.getProperty("innerText"))?.jsonValue()
    );
    return Number(formattedBalance.replace(/[^0-9.-]+/g, ""));
  }
  private async clearPopup() {
    await (await this.page.$(".asset-rate"))?.click();
  }
  public async setChannel(channel: Channel) {
    await this.clearPopup();
    await this.page.waitForSelector("#asset-0");
    const assetContainer = await this.page.$("#asset-0");
    await assetContainer?.click();
    await this.page.waitForSelector(".asset-row");
    const assets = await this.page.$$(".asset-row");
    for (let asset of assets) {
      const name = <string>(
        await (
          await (await asset.$(".name"))?.getProperty("innerText")
        )?.jsonValue()
      );
      if (name.includes("EUR/USD") && channel == Channel.EURUSD) {
        await asset.click();
        break;
      } else if (name.includes("EUR/GBP") && channel == Channel.EURGBP) {
        await asset.click();
        break;
      } else if (name.includes("AUD/USD") && channel == Channel.AUDUSD) {
        await asset.click();
        break;
      }
    }
  }

  public async buy() {
    await this.clearPopup();
    const buyBtn = await this.page.$("#qa_trading_dealUpButton");
    await buyBtn?.click();
  }
  public async sell() {
    await this.clearPopup();
    const sellBtn = await this.page.$("#qa_trading_dealDownButton");
    await sellBtn?.click();
  }
}
