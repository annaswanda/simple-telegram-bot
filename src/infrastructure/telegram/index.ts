import { TelegramClient, Api } from "telegram";
//@ts-ignore
import input from "input"; // npm i input
import { stringSession } from "../../common/utils/telegram/string-session";
import { apiId, apiHash } from "../../common/utils/telegram/api-secrets";
import { Channel, ISignal, Position, Status } from "../../domain/model";
import moment from "moment";
export type TTelegramListener = (signal: ISignal) => void;

export class Telegram {
  private static instance: Telegram;
  private listeners: TTelegramListener[] = [];
  private readonly client = new TelegramClient(stringSession, apiId, apiHash, {
    connectionRetries: 5,
  });
  private persistenceTimeStamp = 0;
  private readonly telegramChannel = "@freesignal4bo";
//   private readonly telegramChannel = "@autotraderlive";
  private _signals: ISignal[] = [];
  private constructor() {
    this.initialize();
  }
  private async initialize() {
    console.log("connecting to telegram...");
    await this.client.start({
      phoneNumber: async () => await input.text("number ?"),
      password: async () => await input.text("password?"),
      phoneCode: async () => await input.text("Code ?"),
      onError: (err: unknown) => console.log(err),
    });
    console.log("telegram connected.");
    console.log(this.client.session.save());
    await this.subscribeForUpdate();
  }

  private async subscribeForUpdate() {
    while (1) {
      const channelDifference = <unknown>await this.client.invoke(
        new Api.updates.GetChannelDifference({
          // channel: "@trade_signal_0",
          channel: this.telegramChannel,
          filter: new Api.ChannelMessagesFilter({
            ranges: [
              new Api.MessageRange({
                minId: 1,
                maxId: 2147483647,
              }),
            ],
            excludeNewMessages: false,
          }),
          pts: new Date().getTime() / 1000,
          limit: 2147483647,
          force: true,
        })
      );
      const channelDifference_ = <Record<string, unknown>>channelDifference;
      if (this.persistenceTimeStamp !== <number>channelDifference_.pts) {
        this.onUpdate();
      }
      this.persistenceTimeStamp = <number>channelDifference_.pts;
    }
  }

  private async onUpdate() {
    const _messages = <unknown>await this.client.invoke(
      new Api.messages.GetHistory({
        // peer: "me",
        peer: this.telegramChannel,
        limit: 1,
      })
    );
    const messages = <Record<string, unknown>>_messages;
    const messageses = <Record<string, unknown>[]>messages.messages;
    const [lastMessage] = messageses;
    const date = this.getDate(lastMessage);
    const message = <string>lastMessage.message;
    if (!message || moment().add(-1, "s").isAfter(date)) {
      return;
    }
    if (message.includes("CALL") || message.includes("PUT")) {
      const signal = {
        channel: this.getChannel(message),
        entryTime: date,
        closeTime: this.getCloseTime(date, message),
        position: this.getPosition(message),
        status: Status.OPEN,
        win: false,
      };
      if (this._signals.length > 100) {
        this._signals.splice(0, 1);
      }
      this._signals.push(signal);
      this.listeners.forEach((listener) => {
        listener(signal);
      });
    }
  }

  private getPosition(message: string): Position {
    const rawPosition = message.split(" ")[0];
    switch (rawPosition) {
      case "CALL":
        return Position.BUY;
      case "PUT":
        return Position.SELL;
      default:
        throw `Unknown Position => [${rawPosition}] => [${message}]`;
    }
  }

  private getDate(lastMessage: Record<string, unknown>) {
    return new Date(Number(`${lastMessage.date}000`));
  }

  private getCloseTime(date: Date, message: string): Date {
    const rawDuration = message.split(" ")[1].split("-")[1];
    let duration = 0;
    if (rawDuration.includes("M")) {
      duration = parseInt(rawDuration) * 60 * 1000;
    }
    return new Date(date.getTime() + duration);
  }

  private getChannel(message: string): Channel {
    if (message.includes("EURUSD")) {
      return Channel.EURUSD;
    } else if (message.includes("EURGBP")) {
      return Channel.EURGBP;
    } else if (message.includes("AUDUSD")) {
      return Channel.AUDUSD;
    } else {
      throw `Unknown Channel => ${message}`;
    }
  }

  public static getInstance(): Telegram {
    if (!Telegram.instance) {
      Telegram.instance = new Telegram();
    }
    return Telegram.instance;
  }

  public subscribe(listener: TTelegramListener) {
    this.listeners.push(listener);
  }

  public unsubscribe(listener: TTelegramListener) {
    this.listeners.splice(this.listeners.indexOf(listener), 1);
  }

  get signals() {
    return this._signals;
  }
}
