import { Binomo } from "./infrastructure/binomo";
import { AutoTradeService } from "./services/auto-trade-service";
const binomo = Binomo.getInstance();
(async () => {
  await binomo.initialize();
  const autoTradeService = new AutoTradeService();
  autoTradeService.start();
  autoTradeService.subscribe(async signal => {
    console.clear();
    console.log(`BALANCE =>`, await binomo.getBalance());
    console.log(signal.unmarshall());
  });
})();
