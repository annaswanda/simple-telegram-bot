"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Telegram = void 0;
const telegram_1 = require("telegram");
//@ts-ignore
const input_1 = __importDefault(require("input")); // npm i input
const string_session_1 = require("../../common/utils/telegram/string-session");
const api_secrets_1 = require("../../common/utils/telegram/api-secrets");
const model_1 = require("../../domain/model");
const moment_1 = __importDefault(require("moment"));
class Telegram {
    constructor() {
        this.listeners = [];
        this.client = new telegram_1.TelegramClient(string_session_1.stringSession, api_secrets_1.apiId, api_secrets_1.apiHash, {
            connectionRetries: 5,
        });
        this.persistenceTimeStamp = 0;
        //   private readonly telegramChannel = "@freesignal4bo";
        this.telegramChannel = "@autotraderlive";
        this._signals = [];
        this.initialize();
    }
    async initialize() {
        console.log("connecting to telegram...");
        await this.client.start({
            phoneNumber: async () => await input_1.default.text("number ?"),
            password: async () => await input_1.default.text("password?"),
            phoneCode: async () => await input_1.default.text("Code ?"),
            onError: (err) => console.log(err),
        });
        console.log("telegram connected.");
        console.log(this.client.session.save());
        await this.subscribeForUpdate();
    }
    async subscribeForUpdate() {
        while (1) {
            const channelDifference = await this.client.invoke(new telegram_1.Api.updates.GetChannelDifference({
                // channel: "@trade_signal_0",
                channel: this.telegramChannel,
                filter: new telegram_1.Api.ChannelMessagesFilter({
                    ranges: [
                        new telegram_1.Api.MessageRange({
                            minId: 1,
                            maxId: 2147483647,
                        }),
                    ],
                    excludeNewMessages: false,
                }),
                pts: new Date().getTime() / 1000,
                limit: 2147483647,
                force: true,
            }));
            const channelDifference_ = channelDifference;
            if (this.persistenceTimeStamp !== channelDifference_.pts) {
                this.onUpdate();
            }
            this.persistenceTimeStamp = channelDifference_.pts;
        }
    }
    async onUpdate() {
        const _messages = await this.client.invoke(new telegram_1.Api.messages.GetHistory({
            // peer: "me",
            peer: this.telegramChannel,
            limit: 1,
        }));
        const messages = _messages;
        const messageses = messages.messages;
        const [lastMessage] = messageses;
        const date = this.getDate(lastMessage);
        const message = lastMessage.message;
        if (!message || (0, moment_1.default)().add(-1, "s").isAfter(date)) {
            return;
        }
        if (message.includes("CALL") || message.includes("PUT")) {
            const signal = {
                channel: this.getChannel(message),
                entryTime: date,
                closeTime: this.getCloseTime(date, message),
                position: this.getPosition(message),
                status: model_1.Status.OPEN,
                win: false,
            };
            if (this._signals.length > 100) {
                this._signals.splice(0, 1);
            }
            this._signals.push(signal);
            this.listeners.forEach((listener) => {
                listener(signal);
            });
        }
    }
    getPosition(message) {
        const rawPosition = message.split(" ")[0];
        switch (rawPosition) {
            case "CALL":
                return model_1.Position.BUY;
            case "PUT":
                return model_1.Position.SELL;
            default:
                throw `Unknown Position => [${rawPosition}] => [${message}]`;
        }
    }
    getDate(lastMessage) {
        return new Date(Number(`${lastMessage.date}000`));
    }
    getCloseTime(date, message) {
        const rawDuration = message.split(" ")[1].split("-")[1];
        let duration = 0;
        if (rawDuration.includes("M")) {
            duration = parseInt(rawDuration) * 60 * 1000;
        }
        return new Date(date.getTime() + duration);
    }
    getChannel(message) {
        if (message.includes("EURUSD")) {
            return model_1.Channel.EURUSD;
        }
        else if (message.includes("EURGBP")) {
            return model_1.Channel.EURGBP;
        }
        else if (message.includes("AUDUSD")) {
            return model_1.Channel.AUDUSD;
        }
        else {
            throw `Unknown Channel => ${message}`;
        }
    }
    static getInstance() {
        if (!Telegram.instance) {
            Telegram.instance = new Telegram();
        }
        return Telegram.instance;
    }
    subscribe(listener) {
        this.listeners.push(listener);
    }
    unsubscribe(listener) {
        this.listeners.splice(this.listeners.indexOf(listener), 1);
    }
    get signals() {
        return this._signals;
    }
}
exports.Telegram = Telegram;
