import { ISignal } from "../../domain/model";
export type TTelegramListener = (signal: ISignal) => void;
export declare class Telegram {
    private static instance;
    private listeners;
    private readonly client;
    private persistenceTimeStamp;
    private readonly telegramChannel;
    private _signals;
    private constructor();
    private initialize;
    private subscribeForUpdate;
    private onUpdate;
    private getPosition;
    private getDate;
    private getCloseTime;
    private getChannel;
    static getInstance(): Telegram;
    subscribe(listener: TTelegramListener): void;
    unsubscribe(listener: TTelegramListener): void;
    get signals(): ISignal[];
}
