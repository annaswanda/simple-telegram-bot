"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Binomo = void 0;
const puppeteer_1 = __importDefault(require("puppeteer"));
const model_1 = require("../../domain/model");
const fs_1 = __importDefault(require("fs"));
class Binomo {
    constructor() {
        this.currentUrl = "";
        this.initialBinomo = {
            host: "https://binomo-go.com",
            email: "adnaw.sanna@gmail.com",
            password: ".Adnawsanna",
            balance: 0,
            trades: [],
            amount: 14000,
            channel: model_1.Channel.EURUSD,
        };
    }
    async initialize() {
        return new Promise(async (res, rej) => {
            try {
                this.browser = await puppeteer_1.default.launch({
                    headless: 'new',
                    args: ["--no-sandbox", "--disable-setuid-sandbox"],
                });
                this.page = await this.browser.newPage();
                await this.useOldCookies();
                await this.page.goto(`${this.initialBinomo.host}`);
                this.currentUrl = await this.page.url();
                this.cookies = await this.saveCookies();
                this.authToken = await this.getAuthToken();
                if (!this.authToken) {
                    await this.page.goto(`${this.initialBinomo.host}/auth`);
                    await this.page.waitForSelector("input");
                    const inputs = await this.page.$$("input");
                    await inputs[0].type(this.initialBinomo.email);
                    await inputs[1].type(this.initialBinomo.password);
                    const button = await this.page.$('button[type="submit"]');
                    await (button === null || button === void 0 ? void 0 : button.click());
                    await this.page.waitForNavigation({ waitUntil: "domcontentloaded" });
                    this.currentUrl = await this.page.url();
                    this.cookies = await this.saveCookies();
                    this.authToken = await this.getAuthToken();
                    if (this.currentUrl === `${this.initialBinomo.host}/trading`) {
                        console.log("Trading NOW");
                        res(true);
                    }
                }
                else {
                    console.log("Trading NOW");
                    res(true);
                }
            }
            catch (e) {
                rej(e);
            }
        });
    }
    async useOldCookies() {
        try {
            const oldCookies = JSON.parse(fs_1.default.readFileSync("./cookies.json").toString());
            await this.page.setCookie(...oldCookies);
        }
        catch (e) { }
    }
    async saveCookies() {
        this.cookies = await this.page.cookies();
        fs_1.default.writeFileSync("./cookies.json", JSON.stringify(this.cookies));
        return this.cookies;
    }
    async getAuthToken() {
        var _a;
        return (_a = this.cookies.find((item) => item.name == "authtoken")) === null || _a === void 0 ? void 0 : _a.value;
    }
    static getInstance() {
        if (!Binomo.instance) {
            Binomo.instance = new Binomo();
        }
        return Binomo.instance;
    }
    async getBalance() {
        var _a;
        await this.page.waitForSelector("#qa_trading_balance", { visible: true });
        const rawBalance = await this.page.$("#qa_trading_balance");
        const formattedBalance = (await ((_a = (await (rawBalance === null || rawBalance === void 0 ? void 0 : rawBalance.getProperty("innerText")))) === null || _a === void 0 ? void 0 : _a.jsonValue()));
        return Number(formattedBalance.replace(/[^0-9.-]+/g, ""));
    }
    async clearPopup() {
        var _a;
        await ((_a = (await this.page.$(".asset-rate"))) === null || _a === void 0 ? void 0 : _a.click());
    }
    async setChannel(channel) {
        var _a, _b;
        await this.clearPopup();
        await this.page.waitForSelector("#asset-0");
        const assetContainer = await this.page.$("#asset-0");
        await (assetContainer === null || assetContainer === void 0 ? void 0 : assetContainer.click());
        await this.page.waitForSelector(".asset-row");
        const assets = await this.page.$$(".asset-row");
        for (let asset of assets) {
            const name = (await ((_b = (await ((_a = (await asset.$(".name"))) === null || _a === void 0 ? void 0 : _a.getProperty("innerText")))) === null || _b === void 0 ? void 0 : _b.jsonValue()));
            if (name.includes("EUR/USD") && channel == model_1.Channel.EURUSD) {
                await asset.click();
                break;
            }
            else if (name.includes("EUR/GBP") && channel == model_1.Channel.EURGBP) {
                await asset.click();
                break;
            }
            else if (name.includes("AUD/USD") && channel == model_1.Channel.AUDUSD) {
                await asset.click();
                break;
            }
        }
    }
    async buy() {
        await this.clearPopup();
        const buyBtn = await this.page.$("#qa_trading_dealUpButton");
        await (buyBtn === null || buyBtn === void 0 ? void 0 : buyBtn.click());
    }
    async sell() {
        await this.clearPopup();
        const sellBtn = await this.page.$("#qa_trading_dealDownButton");
        await (sellBtn === null || sellBtn === void 0 ? void 0 : sellBtn.click());
    }
}
exports.Binomo = Binomo;
