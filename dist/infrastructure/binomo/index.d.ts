import { Channel } from "../../domain/model";
export declare class Binomo {
    private static instance;
    private browser;
    private page;
    private cookies;
    private currentUrl;
    private initialBinomo;
    private authToken;
    private constructor();
    initialize(): Promise<unknown>;
    private useOldCookies;
    private saveCookies;
    private getAuthToken;
    static getInstance(): Binomo;
    getBalance(): Promise<number>;
    private clearPopup;
    setChannel(channel: Channel): Promise<void>;
    buy(): Promise<void>;
    sell(): Promise<void>;
}
