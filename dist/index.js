"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const model_1 = require("./domain/model");
const binomo_1 = require("./infrastructure/binomo");
const auto_trade_service_1 = require("./services/auto-trade-service");
const binomo = binomo_1.Binomo.getInstance();
(async () => {
    await binomo.initialize();
    const autoTradeService = new auto_trade_service_1.AutoTradeService();
    autoTradeService.start();
    autoTradeService.subscribe(async (signal) => {
        await binomo.setChannel(signal.channel);
        if (signal.position == model_1.Position.BUY) {
            await binomo.buy();
        }
        else if (signal.position == model_1.Position.SELL) {
            await binomo.sell();
        }
        // console.clear();
        // console.log(`BALANCE =>`, await binomo.getBalance());
        // console.log(signal.unmarshall());
    });
})();
