export declare enum Channel {
    EURUSD = "EURUSD",
    EURGBP = "EURGBP",
    AUDUSD = "AUDUSD"
}
export declare enum Position {
    BUY = "BUY",
    SELL = "SELL"
}
export declare enum Status {
    OPEN = "OPEN",
    CLOSE = "CLOSE"
}
export interface ISignal {
    channel: Channel;
    entryTime: Date;
    closeTime: Date;
    position: Position;
    status: Status;
    win: boolean;
}
export declare class Signal implements ISignal {
    private props;
    constructor(props: ISignal);
    static create(props: ISignal): Signal;
    unmarshall(): ISignal;
    close(win: boolean): Signal;
    get channel(): Channel;
    get entryTime(): Date;
    get closeTime(): Date;
    get position(): Position;
    get status(): Status;
    get win(): boolean;
}
