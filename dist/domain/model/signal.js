"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Signal = exports.Status = exports.Position = exports.Channel = void 0;
var Channel;
(function (Channel) {
    Channel["EURUSD"] = "EURUSD";
    Channel["EURGBP"] = "EURGBP";
    Channel["AUDUSD"] = "AUDUSD";
})(Channel || (exports.Channel = Channel = {}));
var Position;
(function (Position) {
    Position["BUY"] = "BUY";
    Position["SELL"] = "SELL";
})(Position || (exports.Position = Position = {}));
var Status;
(function (Status) {
    Status["OPEN"] = "OPEN";
    Status["CLOSE"] = "CLOSE";
})(Status || (exports.Status = Status = {}));
class Signal {
    constructor(props) {
        this.props = props;
    }
    static create(props) {
        return new Signal(props);
    }
    unmarshall() {
        return {
            channel: this.channel,
            entryTime: this.entryTime,
            closeTime: this.closeTime,
            position: this.position,
            status: this.status,
            win: this.win,
        };
    }
    close(win) {
        this.props.status = Status.CLOSE;
        this.props.win = win;
        return this;
    }
    get channel() {
        return this.props.channel;
    }
    get entryTime() {
        return this.props.entryTime;
    }
    get closeTime() {
        return this.props.closeTime;
    }
    get position() {
        return this.props.position;
    }
    get status() {
        return this.props.status;
    }
    get win() {
        return this.props.win;
    }
}
exports.Signal = Signal;
