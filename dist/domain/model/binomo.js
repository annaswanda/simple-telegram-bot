"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Binomo = void 0;
const signal_1 = require("./signal");
class Binomo {
    constructor(props) {
        this._props = props;
    }
    static create(props) {
        return new Binomo(props);
    }
    unmarshall() {
        return {
            host: this.host,
            email: this.email,
            password: this.password,
            balance: this.balance,
            trades: this.trades.map((trade) => trade.unmarshall()),
            amount: this.amount,
            channel: this.channel,
        };
    }
    get host() {
        return this._props.host;
    }
    get email() {
        return this._props.email;
    }
    get password() {
        return this._props.password;
    }
    get balance() {
        return this._props.balance;
    }
    get trades() {
        return this._props.trades.map((signal) => signal_1.Signal.create(signal));
    }
    get amount() {
        return this._props.amount;
    }
    get channel() {
        return this._props.channel;
    }
}
exports.Binomo = Binomo;
