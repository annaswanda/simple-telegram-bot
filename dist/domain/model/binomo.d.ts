import { Channel, ISignal, Signal } from "./signal";
export interface IBinomo {
    host: string;
    email: string;
    password: string;
    balance: number;
    trades: ISignal[];
    amount: number;
    channel: Channel;
}
export declare class Binomo implements IBinomo {
    private _props;
    constructor(props: IBinomo);
    static create(props: IBinomo): Binomo;
    unmarshall(): IBinomo;
    get host(): string;
    get email(): string;
    get password(): string;
    get balance(): number;
    get trades(): Signal[];
    get amount(): number;
    get channel(): Channel;
}
