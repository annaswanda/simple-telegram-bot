import { ISignal } from "../domain/model";
import { ISignalRepository, TSignalRepositorySubsriber } from "../domain/repository";
export declare class SignalRepository implements ISignalRepository {
    private _listeners;
    private _telegram;
    constructor();
    onUpdate(signal: ISignal): void;
    subscribe(fn: TSignalRepositorySubsriber): void;
    unsubscribe(fn: TSignalRepositorySubsriber): void;
}
