"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SignalRepository = void 0;
const telegram_1 = require("../infrastructure/telegram");
class SignalRepository {
    constructor() {
        this._listeners = [];
        this._telegram = telegram_1.Telegram.getInstance();
        console.log(this._listeners);
        this._telegram.subscribe(this.onUpdate.bind(this));
    }
    onUpdate(signal) {
        this._listeners.forEach((listener) => listener(signal));
    }
    subscribe(fn) {
        this._listeners.push(fn);
    }
    unsubscribe(fn) {
        this._listeners.splice(this._listeners.indexOf(fn), 1);
    }
}
exports.SignalRepository = SignalRepository;
