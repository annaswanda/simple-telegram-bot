"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AutoTradeService = void 0;
const model_1 = require("../domain/model");
const signal_repository_1 = require("../persistences/signal-repository");
class AutoTradeService {
    constructor() {
        this._listeners = [];
        this.onUpdate = (signal) => {
            this._listeners.forEach((listener) => listener(model_1.Signal.create(signal)));
        };
    }
    start() {
        console.log("Auto Trade Starting...");
        const signalRepository = new signal_repository_1.SignalRepository();
        signalRepository.subscribe(this.onUpdate);
    }
    subscribe(listener) {
        this._listeners.push(listener);
    }
    unsubscribe(listener) {
        this._listeners.splice(this._listeners.indexOf(listener));
    }
}
exports.AutoTradeService = AutoTradeService;
