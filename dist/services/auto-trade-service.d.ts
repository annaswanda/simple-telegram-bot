import { Signal } from "../domain/model";
import { IAutoTradeService } from "../domain/service/auto-trade-service";
export type TAutoTradeServiceListener = (signal: Signal) => void;
export declare class AutoTradeService implements IAutoTradeService {
    private _listeners;
    start(): void;
    private onUpdate;
    subscribe(listener: TAutoTradeServiceListener): void;
    unsubscribe(listener: TAutoTradeServiceListener): void;
}
